+++
comments = false	# set false to hide Disqus
date = 2019-07-23T13:31:45+02:00
draft = false
image = "page/liste-de-lectures-militantes/header.jpg"
menu = "main"		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
slug = "liste-de-lectures-militantes"
tags = []
title = "Liste de lectures militantes"
+++

## Brochures

- [Paranormal Tabou et autres textes sur les violences intracommunautaires](https://infokiosques.net/lire.php?id_article=1198), Anonyme
- [Hot Allostatic Load](https://thenewinquiry.com/hot-allostatic-load/), Anonyme
- [Le Privilège Cissexuel](https://infokiosques.net/lire.php?id_article=884), Julia Serano

## Essais

- [La pensée straight](https://www.placedeslibraires.fr/livre/9782354801755-la-pensee-straight-monique-wittig/), Monique Wittig
- [Refuser d'être un homme](https://www.placedeslibraires.fr/livre/9782849503812-refuser-d-etre-un-homme-pour-en-finir-avec-la-virilite-john-stoltenberg/), John Stoltenberg
- [Troubles dans le consentement](https://www.placedeslibraires.fr/livre/9791025204320-troubles-dans-le-consentement-du-desir-partage-au-viol-ouvrir-la-boite-noire-des-relations-sexuelles-alexia-boucherie/), Alexia Boucherie
- [Manifeste d'une femme trans et autres textes](https://www.placedeslibraires.fr/livre/9782912631251-manifeste-d-une-femme-trans-et-autres-textes-julia-serano/), Julia Serano

---

**Crédit photo**: Photo by [Lê Tân](https://unsplash.com/@ktsfish)
