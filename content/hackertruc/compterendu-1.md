+++
title = "Compte rendu réunion du 8 novembre"
share = false
comments = false
image = ""
slug = "compte-rendu-réunion-du-8-novembre"
date = "2016-11-21T23:05:00+01:00"
author = "hiwelo"
+++

Le groupe projet Hackerspace Inclusif Strasbourg s'est réuni pour la première fois au Quai Numéro 10 à Strasbourg. Merci à eux pour l'accueil dans leurs locaux.
Après la présentation du hackerspace inclusif parisien, le Reset, la réunion a abordé différentes questions au sujet de ce projet :

* Idées d'événements
* Quelle inclusivité et comment ?
* Quelle communication ?
* Quelle organisation ?

## Idées d'événements

Différentes thématiques d'événement ont été proposées, pouvant être regroupée sous la forme de cycles programmatiques.

* Apprendre à cuisiner vegan et échange de recettes
* Apprentissage des technologies liées à l'Arduino
* Initiation aux langages de programmation (cycle entier pour apprendre les bases de chacun et/ou simplement présenter les particularités de chaque langage)
* Organiser des install parties Linux
* Faire des projections débats de film (possiblement non-mixte, en simultanées pour organiser des débats avant une mise en commun en fin de soirée / événement)
* Atelier sur le chiffrement, la sensibilisation aux problématiques de données personnelles, à la cryptographie ou encore aux systèmes de sécurité (caméras, crochetage de serrures, etc.)
* Game design
* Ateliers en extérieur (carte de la vidéo surveillance mise à jour sur Open Street Map, etc.)
* Hacking social, découvrir les problématiques des filtres sociaux, etc.

## Quelle inclusivité ?

La question de la communication autour d'une charte de valeurs type RESET a été posée.
Il faut trouver un moyen de communiquer pour expliquer que les événements sont destinés à tou·te·s quelque soit son niveau.
Les valeurs portées au sein de la charte du RESET étaient plutôt adoptées par tou·te·s pendant cette réunion.
La question de la communication autour des événements non mixte sera à voir. Mais la non mixité semblait un élément indispensable pour certains type d'événements afin d'assurer un espace safe pour tou·te·s.
Il a été rappelé aussi que la non mixité n'est pas uniquement liée aux questions de genre, mais elle pouvait s'appliquer à tout type de caractéristique, comme par exemple un groupe non mixte pour personnes neuro-atypiques.
Il faudra voir pour indiquer le plus précisément possible les publics conviés à chaque événement.

## Quelle communication ?

Les prochaines réunions devront réfléchir aux problématiques de communication, notamment celles-ci :

* Comment communiquer sur la non mixité possible de certains événements ?
* Rappeler que la non mixité possible est un outil qui permet justement la création d'espace safe pour tou·te·s. L'idée n'est pas de privé certains publics de certains événements, il faudra voir dès que possible à proposer ces ateliers dans le cadre de cycle.
* Essayer de sortir des cercles habituels pour attirer des publics qui n'ose pas forcément venir actuellement dans les hackerspaces.
* La charte peut faire peur donc voir comment expliquer qu'il s'agit principalement de valeurs et bon sens permettant à chacun·e de trouver sa place, pas d'un règlement punitif.
* Voir pour ne pas forcément systématiquement communiquer sur la charte, ou alors offrir différents niveaux de lecture de celle-ci.
* Comment présenter à l'avance le déroulement des ateliers, le niveau attendu, expliquer que chacun·e peut y trouver sa place (expert comme débutant), que la collaboration est justement au cœur du projet

Cela pose aussi la question de la communication interne, et donc de comment créer un cercle de confiance au démarrage parmi les personnes au cœur du projet, avant de proposer un espace safe pour tou·te·s.
L'idée de réaliser des petites réunions non mixtes ou en petit comité pour avancer sur certaines parties du projet a été proposée. Cela permettrait à tou·te·s de prendre la parole sans stress.

Une idée de slogan a été proposée à l'issue de la réunion : "On est tous légitimes à apprendre ou à partager"

## Quelle organisation ?

Il a été décidé de ne pas monter d'organisation particulière pour le moment, autre que la mailing-list et les réunions informelles comme celle en cours.
Les prochaines réunions permettront de définir quels événements peuvent être organisés par les personnes présentes, les lieux et la fréquence de ces possibles événements.
