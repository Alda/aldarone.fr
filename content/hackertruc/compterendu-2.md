+++
title = "Compte rendu réunion du 13 décembre"
slug = "compte-rendu-réunion-du-13-décembre"
share = false
comments = false
image = ""
date = "2017-03-24T12:25:01+01:00"
+++

La seconde réunion du groupe projet Hackerspace inclusif de Strasbourg a à nouveau eu lieu au Quai Numéro 10 à Strasbourg. Après un rapide tour de présentation pour les personnes qui n'étaient pas là à la précédente, nous avons décidé à l'unanimité des présent⋅es que le hackerspace s'appellerai **Le HackerTruc**

Nous avons ensuite réfléchi sur les ateliers pouvant être organisés par les personnes présentes ainsi que sur les lieux à aborder pour la tenue de ces ateliers.

## Les Ateliers

- L'intérêt de certaines personnes n'est pas forcément numérique ou sur des sujets consensuels, comme par exemple à destination des populations à risque vis à vis de l'état d'urgence.

- On peut mettre à jour l'emplacement des caméra de surveillance vu que des bornes de trafic ont été ajoutées avec les caméras correspondantes pour les surveiller, ainsi que les rénovations de station de tram. Ça donnera l'occasion de faire une activité en extérieur, et de trouver des trajets sans caméra.

- Même si certaines activités ont l'air illégales ou limite il y a toujours un versant légal sur lequel on peux communiquer par exemple le crochetage de serrure ou l'explication du fonctionnement de la video surveillance.

- Partir d'activités concretes et faire le lien avec des choses plus abstraites parce que c'est plus faciles d'être face à quelqu'un qui explique plutôt que lire un tuto sur internet.

- Quelqu'un se propose de faire un atelier hacking de jouets connectés pour détourner l'animatronique puis améliorer l'électronique pour des usages différents comme brouiller des signaux radio, etc.

- Il y a possibilité d'organiser un atelier cuisine végane en pratiquant du freeganisme.

- Animation d'un atelier d'intro à la programation et à la création de site web.

- Atelier de protection sur les wifi publics ou des wifi dans les hotels etc.

## Les lieux

- Le Shadok est un lieu difficile d'accès en raison du public qu'on cible, des gestionnaires et de la présence du Hackerstub.

- Idem pour la Station qui est également trop ancré dans le paysage politique Strasbourgeois.

- Le Molodoï est un lieu qui semble plus accueillant au niveau des affinités politiques des présent⋅es mais il faut arriver à dépasser l'équipe d'organisation du lieu.

- Il faudrait retrouver les tumultueuses pour savoir où elles organisaient leurs trucs.

- La Perestroïka ou La Taverne Française laissent assez facilement accès à sa véranda aux associations.

- On peut aussi envisager la maison Mimir quand illes pourront à nouveau accueillir du public.

- Le Quai N°10 est dispo quand il y a un lien avec le numérique et que quelqu'un de l'assoc' est là pour faire le portier.
