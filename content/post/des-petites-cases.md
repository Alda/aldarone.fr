+++
title = "Des petites cases"
slug = "des-petites-cases"
image = "/img/samuel-zeller-367971-unsplash.jpg"
comments = false	# set false to hide Disqus
share = true	# set false to hide share buttons
date = 2018-04-18T02:07:33+02:00
+++

Un des trucs que j'ai le plus entendu ces dix dernières années, depuis que je
suis militante en fait, c'est cette question à la con qui revient sans cesse :
« Mais pourquoi est-ce que tu te sépares du reste des gens en te mettant
dans une case ? »

Il paraît qu'on est toutes dans le même bateau. Il paraît que les étiquettes
c'est réducteur. Il paraît que les cases ça divise.

On me dit que c'est moi qui me met dans des cases. Que c'est ma faute si je
me retrouve acculée en dehors de la normalité.

Ben ouais, c'est pas à cause des autres si t'as décidé de mettre une jupe alors
que t'es un mec. Et qu'on te traite de tafiole pour ça.

Ben ouais, c'est pas à cause des autres si t'as laissé ta meuf tomber amoureuse
d'un autre type sans que ça détruise ton couple. Et qu'on te traite de pervers
pour ça.

Hé ouais ! C'est pas à cause des autres si t'as décidé d'être une femme. Et
qu'on te traite de folle, monstre, pédé, pute, crève pour ça.

Si les cases c'est aussi nul, arrête donc de vouloir me faire rentrer dans la
tienne. Celle des Gens Normaux® qui font chier que celles qui sont pas dans
La Case®.

En même temps je comprends. Elle est tellement étriquée, c'est tellement
mortellement chiant d'y être que tu crèves sans doute de jalousie devant moi
et les autres qui faisons tout pour en sortir.

Et si nous on en sort et pas toi tu sens bien que tu passes pour un con. Alors
tu nous en crée des nouvelles pour nous y foutre dedans. Tafiole, Pédé, Pute,
Monstre, Femme.

Sauf que nous on s'en fout des cases en vrai. On veut juste pas être dans la
tienne. Elle est trop petite, on étouffe là dedans. Toi aussi t'étouffes là
dedans.

Alors on voit le côté pratique des cases. Vu que t'as viré tous les gens un
peu différents de La Case® ben on se retrouve entre différentes. Et on voit ce
qu'on a en commun. On se rencontre et on s'organise pour niquer ta Case® et
que t'arrêtes de nous y faire chier.

T'as bien compris qu'ensemble on est plus fortes. C'est pour ça que t'as peur
de voir nos cases se remplir. C'est pour ça que tu sens bien qu'un jour les
cases on va les faire péter. Surtout la tienne.

#### Crédit image

Photo by Samuel Zeller on Unsplash:
<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@samuelzeller?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Samuel Zeller"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Samuel Zeller</span></a>
