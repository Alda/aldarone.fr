+++
share = true
comments = false
date = "2017-04-02T12:12:07+02:00"
title = "Welcome to Mastodon"
image = "/img/mastodon-join-the-federation.jpg"
slug = "welcome-to-mastodon"
+++

Depuis quelques jours, Mastodon reçoit entre 50 et 100 inscrit⋅es par heure et on peut voir sur twitter quelques messages enthousiastes incitant plus de monde à migrer sur cette alternative « Libre et Décentralisée. »

## C'est quoi ce truc ?

Mastodon est un logiciel accessible par un navigateur et des applications iOS ou Android qui vise, par ses fonctions de base, le même public que Twitter.

L'interface est très similaire à celle de Tweetdeck, on suit des comptes, des comptes nous suivent, on a une timeline, des mentions, des hashtags, on peut mettre un message dans nos favoris et/ou le partager tel quel à nos abonné⋅es. Bref, tout pareil. Même les comptes protégés et les DMs sont là. (À l'heure actuelle il ne manque que les listes et la recherche par mots clés.)

Il y a quelques fonctionnalités supplémentaires que je détaillerai par la suite mais la différence de taille réside dans ce « Libre et Décentralisé » que tout le monde répète à l'envi et qui peut rendre les choses confuses quand on ne voit pas de quoi il s'agit.

## Le Fediverse : Un réseau décentralisé

Mastodon est donc un logiciel. Au contraire de Twitter qui est un service. Personne ne peut installer le site Twitter sur son ordinateur et permettre à des gens de s'inscrire et d'échanger ailleurs que sur twitter.com. Par contre quiconque a les connaissances nécessaire peut télécharger Mastodon, l'installer quelque part et le rendre accessible à d'autres.

C'est ce qui se passe déjà avec les sites suivants :

- mastodon.social
- icosahedron.website
- mastodon.xyz

Ces trois exemples sont des sites différents (on les appelle des « instances ») à partir desquels il est possible de rejoindre le réseau social appelé « Fediverse » (mais comme c'est pas très joli on va dire qu'on « est sur Mastodon » hein ?)

C'est là que se trouve toute la beauté du truc : toute personne inscrite sur n'importe laquelle de ces instances peut discuter avec les personnes inscrites sur les deux autres de manière transparente. Et tout le monde est libre d'en créer de nouvelles et de les connecter ou non avec les autres.

Pour résumer, on s'inscrit sur une instance de Mastodon, cette instance est dans un réseau appelé le Fediverse et les gens qui sont dans le Fediverse peuvent échanger entre eux.

Comme personne ne peut contrôler l'ensemble du réseau puisqu'il n'y a pas d'instance centrale, on dit que c'est un réseau décentralisé. Et quand une instance se connecte aux autres instances on dit qu'elle « fédère » avec les autres.

Si cette histoire d'instance est encore trop nébuleuse, imaginez un email. Vous êtes Alice et votre fournisseur de mail est Wanadoo. Votre addresse mail est donc alice@wanadoo.fr. Vous avez un ami nommé Bob qui est chez Aol et son adresse mail est bob@aol.com. Alors que vos fournisseurs respectifs sont différents, ils peuvent communiquer et vous pouvez ainsi envoyer des messages à Bob avec votre adresse mail de Wanadoo. Mastodon fonctionne selon le même principe, avec les instances dans le rôle du fournisseur.

## Pourquoi c'est mieux que Twitter ?

Maintenant qu'on a évacué la partie un peu inhabituelle et pas forcément simple à comprendre, on peut attaquer les fonctionnalités de Mastodon qui donnent bien envie par rapport à Twitter.

### La base

En premier lieu jettons un œil à l'interface :

![Interface de Mastodon](/img/mastodon.png)

Si on utilise Tweetdeck on est pas trop dépaysé puisque l'interface s'en inspire fortement. La première colonne est la zone de composition, c'est ici qu'on écrit nos Pouets (c'est le nom Mastodonien des Tweets), qu'on décide où les poster et qu'on y ajoute des images.

La seconde colonne c'est « la timeline », ici s'affichent les pouets des personnes qu'on suit.

La troisième colonne c'est les notifications qui contiennent les mentions, les boosts (sur Twitter on dit RT) et les favoris qu'on reçoit.

La quatrième colonne a un contenu variable selon le contexte et les deux premières possibilités méritent leur explication :

- **Local timeline** : Ce mode affiche tous les pouets publics de l'instance sur laquelle on se trouve, même des gens qu'on ne suit pas.
- **Federated timeline** : Ce mode affiche tous les pouets publics de toutes les instances fédérées avec celle sur laquelle on se trouve. Ce n'est pas forcément tous les pouets publics du Fediverse, mais ça s'en approche.

Pour suivre et être suivi le fonctionnement est identique à celui de Twitter : on affiche un profil en cliquant sur son nom et on peut le suivre. Si le profil est « protégé » il faut que son ou sa propriétaire valide la demande.

Enfin, un pouet peut faire jusqu'à 500 signes de long au lieu des 140 de Twitter.

### La confidentialité des pouets

Contrairement à Twitter où les comptes publics font des tweets publics et les comptes protégés font des tweets protégés, Mastodon permet à chacun de décider qui pourra voir un pouet.

![Les 4 niveaux de confidentialité des pouets](/img/mastodon-pouet-confidentialite.png)

Le premier niveau est **public**, tout le monde peut voir le pouet et il s'affichera également dans les parties « Local Timeline » et « Federated Timeline » de la quatrième colonne.

Le niveau deux est **unlisted**, c'est comme un pouet public mais il ne s'affichera ni dans la timeline Locale ni dans la timeline Federated.

Le niveau trois est **private**, c'est à dire visible uniquement par les gens qui nous suivent. C'est le niveau équivalent à celui des tweets envoyés par des comptes protégés sur Twitter.

Le niveau quatre est **direct**, les pouets ne seront visibles que par les personnes mentionnées à l'intérieur. Ça correspond aux DMs de Twitter sauf que les pouets sont directement intégrés dans la timeline au lieu d'être séparés des autres pouets.

Bien sûr, il est possible de changer le niveau individuel d'un pouet avant de l'envoyer.

### Avoir un compte protégé sur Mastodon

Comme sur Twitter, il est possible de protéger son compte, c'est à dire de valider les gens qui s'abonnent. Cependant, on peut toujours définir certains de nos pouets comme étant publics.

Plus besoin d'avoir deux comptes pour poueter en privé !

### Une gestion native du Content Warning et des images NSFW

Une pratique courante sur Twitter est de préciser en début de tweet les éventuels trigger warning qui y sont associés, mais le reste du tweet reste visible.

Mastodon généralise le concept en permettant de saisir une partie visible et une partie masquée à nos pouets. On peut ainsi y mettre des messages potentiellement trigger, nsfw, spoilers ou autres.

![Rédaction d'un pouet en Content Warning](/img/mastodon-pouet-content-warning.png)

![Affichage caché d'un pouet en Content Warning](/img/mastodon-pouet-content-warning-1.png)

![Affichage visible d'un pouet en Content Warning](/img/mastodon-pouet-content-warning-2.png)

De même quand on poste une image, on peut la déclarer comme étant NSFW, ce qui nécessite de cliquer dessus pour l'afficher :

![Rédaction d'un pouet avec une image NSFW](/img/mastodon-pouet-nsfw.png)

![Affichage masqué d'un pouet avec une image NSFW](/img/mastodon-pouet-nsfw-1.png)

![Affichage visible d'un pouet avec une image NSFW](/img/mastodon-pouet-nsfw-2.png)

## Ok, vendu, du coup comment ça se passe ?

Tout d'abord il faut choisir sur quelle instance s'inscrire puisque, ayant des propriétaires différents, il est possible qu'elles aient des règles différentes dont il convient de prendre connaissance au préalable.

L'existence de la « Local Timeline » est intéressante à ce niveau puisque son contenu diffère forcément selon l'endroit où on est inscrit⋅e. Par exemple si on va sur une instance à tendance germanophone, il est à peu près sûr que la plupart de ce qu'on y trouvera sera en allemand.

Ça ouvre tout un tas de possibilités comme la constitution d'instances orientées en fonction d'un fandom, d'intérêts politiques et/ou associatifs.

Par exemple, l'instance awoo.space est volontairement isolée du reste du Fediverse (elle ne communique qu'avec l'instance mastodon.social) et la modération se fait dans le sens d'un fort respect des limites personnelles de chacun⋅e.

On peut trouver une [liste d'instances connues sur le dépôt de Mastodon](https://instances.mastodon.xyz) et à l'exception d'awoo.space il est possible de parler au reste du Fediverse depuis n'importe laquelle figurant sur cette liste, il n'y a donc pas de forte obligation d'aller sur la même instance que nos potes puisqu'on pourra leur parler de toute façon.

Une fois inscrit⋅e, on aura un identifiant qui ressemble un peu à une addresse mail et qui servira à nous reconnaître sur le Fediverse. Cet identifiant dépend du pseudo choisi et du nom de l'instance. Ainsi, je suis **Alda** sur l'instance *witches.town*, mon identifiant est donc **Alda@witches.town**.

Pour trouver des gens à suivre, on peut se présenter sur le tag #introduction (avec ou sans s) et suivre un peu la Federated Timeline. On peut aussi demander leur identifiant à nos potes et le saisir dans la barre de recherche de la première colonne.

![Recherche d'utilisateur Mastodon](/img/mastodon-user-search.png)

Et voilà, il n'y a plus qu'à nous rejoindre par exemple sur :

- [icosahedron.website](https://icosahedron.website/about)
- [mastodon.xyz](https://mastodon.xyz/about)

Ou à créer votre propre instance pour agrandir le Fediverse !

Évitez par contre de rejoindre l'instance mastodon.social. Elle est assez saturée et l'intérêt de la décentralisation réside quand même dans le fait de ne pas regrouper tout le monde au même endroit. Même si vous connaissez des gens qui y sont, vous pourrez quand même les suivre depuis une autre instance.

## Point Bonus

### Des applis mobiles

Pour faire pouet avec Android : [Installe Tusky](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky)

Pour faire pouet avec iOS : [Installe Amaroq](https://itunes.apple.com/us/app/amarok-for-mastodon/id1214116200?mt=8)

### Retrouver ses potes de Twitter

Le développeur principal de Mastodon a aussi fait une application pour retrouver ses potes de Twitter. Il faut se rendre sur [Mastodon Bridge](https://mastodon-bridge.herokuapp.com/), se connecter avec Mastodon et Twitter et le site affichera ensuite les comptes correspondants qu'il aura trouvé.

#### Crédit image

[Join the Federation](https://mastodon.social/@b_cavello/1429836) par [B! Cavello](https://mastodon.social/@b_cavello)
