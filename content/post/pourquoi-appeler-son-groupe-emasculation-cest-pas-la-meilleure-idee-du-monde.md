+++
share = true
comments = false
date = 2018-01-02T17:09:44+01:00
title = "Pourquoi appeler son groupe de punk « Emasculation » c'est pas la meilleure idée du monde ?"
image = "/img/natanael-melchor-463647.jpg"
slug = "pourquoi-appeler-son-groupe-de-punk-emasculation-cest-pas-la-meilleure-idee-du-monde"
+++

En ce moment La Mutinerie organise des soirées de soutien pour récolter des
fonds et à la prochaine il y aura un groupe nommé « Emasculation » composé de
deux meufs cis. Du coup je vous explique pourquoi c'est une très mauvaise idée
d'appeler votre groupe comme ça si vous êtes des cis :

Je comprends tout à fait l'envie de s'en prendre de manière violente aux mecs
cis heteros qui nous pourrissent la vie et l'évidence qu'il y a à s'attaquer de
front à leur plus cher symbole de virilité, mais les conséquences en terme
d'essentialisme et de transphobie sont assez atroces.

Déjà, placer le focus sur le génital c'est essentialiste. Ça fait perpète qu'on
devrait toustes avoir admis que le genre étant une construction sociale ça ne
sert à rien de parler de bites et de chattes dans les revendications politiques
parce que ça laisse sur le carreau toutes les personnes trans. L'autre versant
de la diabolisation du pénis c'est la célébration des seins, du vagin et de
l'utérus, les attributs de « la femme génitrice », vous admettrez que ça pue
tout autant…

Ensuite parler de couper des bites, ça nous fait frissonner nous meufs trans
parce que ça nous renvoie au fait qu'avec une bite on serait pas des femmes,
peut-être même des hommes infiltrés (du point de vue TERF), et la stérilisation
forcée ça se passe encore en France quand on veut des papiers (oui même si
c'est maintenant illégal il y a encore des juges qui réclament ce genre de
choses pour valider un changement d'état civil et c'est encore bien bien ancré
dans la société)

Enfin, ça promeut l'idée que le truc le plus humiliant pour un mec c'est de pas
avoir de bite, là c'est les mecs trans qui frissonnent et qui se prennent toute
la transphobie du truc dans la gueule.

Je connais pas les meufs d'Emasculation et je veux bien croire que quand elles
ont décidé de s'appeler comme ça ça semblait cool et ça ferait rager du mascu.
Mais le prix à payer n'en vaut clairement pas la chandelle et j'espérais un peu
que les orga de la Mut' auraient la conscience politique pour se rendre compte
du problème d'en discuter avec elles et de pas nous infliger ça.

#### Crédit image

Photo by Natanael Melchor on Unsplash : <a style="background-color:black;color:white;text-decoration:none;padding:4px
6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;,
&quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe
UI&quot;, Arial,
sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;"
href="https://unsplash.com/@nmelchorh?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge"
target="_blank" rel="noopener noreferrer" title="Download free do whatever you
want high-resolution photos from Natanael Melchor"><span
style="display:inline-block;padding:2px 3px;"><svg
xmlns="http://www.w3.org/2000/svg"
style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;"
viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2
4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8
4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3
1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8
2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5
7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span
style="display:inline-block;padding:2px 3px;">Natanael Melchor</span></a>
