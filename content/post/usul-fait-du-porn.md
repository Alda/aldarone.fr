+++
title = "Usul fait du porn"
slug = "usul-fait-du-porn"
image = "/img/cloudvisual-208962-unsplash.jpg"
comments = false
share = true
date = 2018-02-20T10:19:45+01:00
draft = true
+++

Depuis quelques jours, une tempête émanant de la fachosphère fait rage autour
d'Usul au motif qu'il participe depuis peu aux stream de sa copine, qui est
camgirl.

Ça c'est un peu un non évènement, des camgirls qui font du sexe avec leur
copain sur pornhub j'ai envie de dire qu'on s'en tamponne les oreilles et il
n'y a guère que des rageux pour considérer qu'il y a quoi que ce soit de
honteux là dedans.

Ensuite, apparemment ils vont remettre le couvert parce que ça leur a plu et
qu'ils ont envie de recommencer. Bon soit.

Ce qui m'emmerde vachement plus par contre c'est la politique « pro-sexe »
qu'Olly met derrière et qu'Usul ne manquera pas de récupérer parce qu'en étant
associé à ça, il peut rien faire d'autre :

- Soit il récupère le discours et c'est un mec cis hétéro qui promeut le fait de
    se taper une pornstar.
- Soit il rejette le discours et c'est un mec cis hétéro qui mecxplique à une
    pornstar la position politique qu'elle tient elle même.

Leur problème c'est qu'ils sont dans cette situation inextricable dont ils ne
peuvent sortir qu'en arrêtant ça parce qu'ils sont tous les deux hétéros et que
lui est une figure publique politique.

Sa simple participation relève du discours politique de promotion de
l'hétérosexualité. Il ne peut pas s'agir d'autre chose « qu'un homme valorisé
parce qu'il couche avec un corps. » aka : La reproduction brute du privilège
masculin.

Il est d'autant plus valorisé qu'il possède une certaine notoriété. Les fachos
qui l'engueulent, ses fans, ses soutiens, tous le jalousent et l'envient.

C'est bien pour ça que lui s'en prend plein la gueule sur des positions et sur
du matériel alors qu'elle subit du sexisme et de la putophobie dégueulasse.

Et déso pas déso, quand on a des prétentions féministes, on ne se met pas dans
une telle situation de domination. Et on inflige pas ça à une meuf à qui on
tient intimement.

J'vais afficher une unpopular opinion mais le féminisme pro-sexe, dans son absence
de lutte contre l'hétérosexualité, ne sert qu'à augmenter la disponibilité du
sexe des femmes pour les hommes sous couvert de « libération sexuelle. »

Il serait peut-être temps de dépasser ça et d'analyser la sexualité sous
l'angle du [féminisme sex-negative][1] qui a beaucoup de choses à dire sur la
domination patriarcale de notre sexualité.

[1]: https://radtransfem.wordpress.com/2012/02/29/the-ethical-prude-imagining-an-authentic-sex-negative-feminism/

La réappropriation de nos corps ok, mais est-ce qu'on a vraiment envie de faire
ça pour mieux les servir sur un plateau aux mecs cis ?

#### Crédit image

Photo by CloudVisual on Unsplash : <a style="background-color:black;color:white;text-decoration:none;padding:4px
6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica,
Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;
display:inline-block;border-radius:3px;"
href="https://unsplash.com/@cloudvisual?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge"
target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from CloudVisual">
<span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg"
style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32">
<title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8
2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3
4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6
7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span>
<span style="display:inline-block;padding:2px 3px;">CloudVisual</span></a>
