+++
title = "Un peu de solidarité trans"
slug = "un-peu-de-solidarite-trans"
date = 2018-04-25T07:26:28+02:00
image = "/img/wolf-pack-mentality-560768933.jpg"
comments = false	# set false to hide Disqus
share = true	# set false to hide share buttons
+++

Aujourd'hui je vais te parler d'un truc qu'on voit clairement pas assez. Ça
s'appelle la solidarité trans. J'ai envie de t'en parler parce qu'une fois de
plus j'ai vu un mec trans faire l'avocat du fait que ce serait shitty de faire
une place aux mecs trans dans le féminisme.

Il parait que faire une place aux mecs trans dans le féminisme c'est oublier
que les mecs trans sont des mecs. Il parait que c'est oublier que les mecs
trans toxiques seraient pires que beaucoup de mecs cis. Il parait que c'est de
la transphobie bienveillante.

Alors d'abord je vais t'énoncer un truc : Le féminisme cis est transphobe. Le
féminisme cis essentialise la masculinité et considère qu'elle laisse une trace
indélébile sur toute les personnes qu'elle touche. Cette trace, je la soupçonne
d'être une trace de testo.

## Le féminisme cis

La transphobie du féminisme cis on la connait bien quand on est une meuf trans
lesbienne. Les meufs cis se crispent sur notre passage, elles font tout leur
possible pour nous refuser l'accès aux lieux non-mixtes en prétendant protéger
les victimes d'agressions sexuelles, elles hésitent pas à nous qualifier de «
prédateurs » sexuelles pour ça.

Et elles invoquent ici la présence d'organes génitaux indésirables, là notre
prétendue socialisation masculine. Mais la manière dont elles traitent les mecs
trans prouve que c'est pas ça qui compte pour elles.

Contrairement aux meufs trans qui sont mises dans le même sac (à l'exception
peut-être de quelques tokens hétéro dociles), les assignés meufs sont divisés
en deux catégories grossières :

- Le mec en début de transition avec un passing aléatoire, il a pas encore trop
  de poils, il a pas encore mué, il a l'air pipou et inoffensif.
- Le mec toxique qui ouvre sa gueule et qui est presque pire que beaucoup de
  mecs cis. Il a bien pris sa testo, ça se voit. Il a les traits plus marqués,
  il a mué, il a plus (trop) peur dans la rue.

Là tu devrais commencer à cerner le problème. Les mecs qui sont dans la
deuxième catégorie c'est comme les meufs trans. Des vrais mecs qui prennent
trop de place. Parce qu'ils ont la testo.

La testo qui infuse les gens avec son pouvoir de masculinisation et qui les
transforme en prédateurs sexuels prêts à tout.

Ben laisse moi te dire que c'est de la merde en barre.

Ça devrait être évident pour tout le monde que personne se balade avec les
résultats de sa dernière prise de sang hormonale. Et donc que personne sait
quel est le taux d'hormone du ou de la trans en face de soi.

Parce que toute cette saloperie c'est une question de passing.

## La testo c'est le passing

Le passing c'est cette notion qui va déterminer si une meuf trans est assez
meuf. Ou si un mec trans est trop mec. Ouais t'as vu ça marche à l'envers selon
si t'es une meuf ou un mec.

Faire la police du passing, c'est complètement zapper toute prise en compte de
transidentité pour appliquer des critères cis sur des individus pour compliquer
ou empêcher leur transition.

Concrètement, il s'agit de foutre la barre du passing à une hauteur
inimaginable pour les meufs trans pour que chaque attitude, manière d'être, de
se vetir, de se maquiller, de s'exprimer puisse etre une occasion de les
renvoyer à leur genre de naissance et invalider nos transitions.

Tu parles trop fort ou avec une voix trop grave ? C'est la testo, au fond t'es
un mec.

T'as la mâchoire carrée ? C'est la testo, au fond t'est un mec.

Tu parles de cul ? C'est la testo, au fond t'es un prédateur.

Par contre, au contraire, il s'agit de foutre la barre du passing hyper bas
pour les mecs trans pour que chaque attitude, manière d'être, de se vêtir, de
se maquiller, de s'exprimer puisse être une occasion de les  accuser d'avoir
une masculinité toxique.

Tu parles pas assez doucement, tu t'énerves ? C'est la testo, t'es un vrai mec.

T'as de la barbe ? C'est la testo, t'es un vrai mec.

Tu prends « trop » de place ? C'est la testo, t'es un vrai mec.

Et tant pis si tu deviens fou parce que tu voulais quitter un monde
d'injonctions sexistes et au final on te flique quand même au moins autant. Et
tant pis si tu subis la violence médicale et administrative en plus de ça. Non.
Déso, t'es un mec.

Et on soutient pas les mecs.

## L'essentialisation de la masculinité est une transphobie

Le but bien sur, c'est de nous empêcher de transitionner et de nous exclure
quand on le fait. Si on se plie au jeu du passing, on a toutes les chances de
se cramer complètement tellement les attentes du cistème sont intenables.

Ça dit aux meufs trans qu'elles auront beau essayer, elles y arriveront de
toute façon jamais. Ou alors à quel prix ?

Mais dans le cas dont il est question ici, ça dit surtout aux assignés meufs,
qui subissent le sexisme, la violence physique et morale, que si ils ont le
malheur de trahir ils pourront bien crever la gueule ouverte on les aidera pas.
Parce qu'on oubliera pas « que ce sont des mecs »

Il faut qu'ils aient peur de transitionner. Il faut qu'ils réaffirment
constamment qu'ils ne sont pas solidaires avec les mecs cis. À la limite ils
peuvent se montrer rassurant en rappelant constamment qu'ils ne sont pas
vraiment des mecs.

Cette focalisation sur l'individualité du passing et sur l'identité personnelle
de chacun permet d'évacuer la question de l'oppression et du pouvoir politique
sans même y penser.

## Les trans n'ont pas de privilège de genre

Répéter que les meufs trans sont des meufs comme les autres et que les mecs
trans sont des mecs comme les autres c'est à peu près aussi transphobe que de
dire que les meufs trans sont des mecs et que les mecs trans sont des meufs.

Les meufs trans sont pas des meufs cis. Les mecs trans sont pas des mecs cis.
Et on le sera jamais et on a même intérêt à ne meme pas essayer d'en devenir.
On est intrinsèquement inscrit⋅es dans la transgression. Que ce soit parce que
notre transition est visible comme le nez au milieux de la figure et que ce
sera jamais assez bien pour les cis, ou que ce soit parce qu'on vit dans la
peur constante d'être découvert⋅es et identifié⋅es comme trans parce que c'est
jamais assez bien pour les cis.

Les meufs trans sont des meufs **trans**. On se bouffe du sexisme et de la
transphobie. Les mecs trans sont des mecs **trans** ils se bouffent ou se sont
bouffés du sexisme et se bouffent encore de la transphobie.

Les mecs trans n'ont aucun pouvoir politique. Ils sont invisibles dans les
médias et inexistants dans les sphères dirigeantes. Même au sein du féminisme
ils sont scrutés comme les autres trans. Et comme les autres trans ils sont
plus exposés aux agressions et au suicide.

Le patriarcat c'est pas un truc avec les mecs aux dessus qui exploitent les
meufs en dessous. Le patriarcat c'est un truc avec les mecs **cis** qui
exploitent les **non**-mecs cis et il est complètement illusoire de s'imaginer
que les mecs cis seront prêt à pactiser avec les mecs trans pour maintenir la
domination cismasculine.

La solution à tout ça, c'est d'arrêter de se fliquer.

## Mort aux flics dans nos têtes

ACAB ça vaut pas que pour les connards en uniforme qui assassinent à tour de
bras impunément dans la rue. ACAB ça vaut aussi pour le cis qu'on a dans la
tête.

Celui qui nous murmure en permanence que là on a l'air d'un bonhomme. Qu'on est
pas assez une meuf. Qu'on est trop un mec.

Celui aussi qui nous murmure que machin quand même, il l'a vachement ouvert
l'autre jour. Et puis machine qui fait aucun effort sur sa voix qui couvre tout
dès qu'elle l'ouvre.

Celui qui nous fait penser que les mecs trans ils sont plus toxiques que
beaucoup de mecs cis. Sauf ceux qui sont pipous. Mais attention à ceux qui sont
pipous, ils ont intéret à le rester.

Sans flics, on vivra mieux.

## Pour une solidarité trans

Sans flics, on transitionnera mieux.

Au lieu de se mettre des buts inatteignables entre meufs trans, on ferait mieux
de se foutre la barre au minimum. Pour qu'aucune meuf trans n'ait à dépasser la
peur viscérale de prendre une place qui n'est pas la sienne. Franchement, si tu
penses que t'es une meuf, c'est probablement que t'en est une. On s'en fout de
ton passing, on s'en fout que tu puisses pas être out au boulot ou en famille.
La seule étape, c'est d'admettre que tu veux pas de tes privilèges cismasculins
et que tu t'épanouirais vachement plus si t'étais une meuf.

Et au lieu de jeter nos camardes sur le bucher des mecs cis dès qu'ils haussent
un peu la voix, on devrait les rassurer. Leur dire qu'on les laissera pas
tomber quand ils se prendront de la merde parce qu'ils sont autant victimes du
patriarcat que les autres non-mecs cis. Et qu'ils ont pas besoin d'avoir peur
de nous et de nous rassurer en permanence non non je suis pas comme les mecs
cis, non non je suis gentil, non non je comprends regarde je suis pipou les
mecs ils sont pas pipous moi je suis même pas tant un mec que ça d'ailleurs.

On niquera pas le genre si on laisse ses injonctions nous bouffer.

#### Crédits

Merci aux camarades pour la relecture.
