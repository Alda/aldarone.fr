+++
comments = false	# set false to hide Disqus
date = 2019-07-29T10:32:44+02:00
image = "/jécris-ton-nom-vasectomie-2-3-mise-en-place/img/header.jpg"
share = true	# set false to hide share buttons
slug = "jécris-ton-nom-vasectomie-2-3-mise-en-place"
title = "J'écris ton nom, vasectomie (2/3) : Mise en place"
+++

C’est l’histoire d’une meuf de trente-deux ans qui a eu une vasectomie il
y a un peu plus de quatre-vingt-seize heures et qui en peu plus de larver sur
son canapé en attendant que ses poils repoussent alors elle décide de
raconter comment ça lui est arrivé.

### Mise en place

Une matin de désœuvrement intense je profite d'un pic de motivation pour me
dégotter une urologue et prendre rendez-vous. Je sors le combo gagnant
Ameli/Doctolib et je choppe un rendez-vous pour le huit janvier sans même avoir
à parler à qui que ce soit.

Mme Louise de Beaufort pour une vasectomie. Je me demande si quelqu'un a cru
que j'étais une hétéro qui voulait couper les couilles de son mec cis.

!["The female hormone monster doing her evil laugh"](https://media.giphy.com/media/xUPJUtz6yWG6brN9zq/giphy-downsized.gif)

Le moment venu je me fais accompagner par une amoureuse et on se met en route
vers le cabinet de ma future urologue qui se trouve dans un des quartiers les
plus bourge de Strasbourg, dans une rue qui borde le parc de l'Orangerie
(excusez du peu)

À l'arrivée, la secrétaire a effectivement un peu de mal. Déjà elle se retrouve
devant une meuf qui lui dit qu'elle vient pour une vasectomie mais en plus la
meuf en question a pris rendez-vous avec son prénom d'usage mais c'est pas le
premier qui est noté sur sa carte d'identité et sa carte vitale porte encore
son morinom[^1].

Ça fait visiblement trop d'info d'un coup alors dans un élan de compassion je
lui autorise à mettre mon dossier au masculin, en plus ça m'évitera
sûrement des embrouilles avec la sécu.

La radio dans la salle d'attente diffuse TopMusic, les toiles accrochées aux
murs représentent des graffitis et on est installée en face d'un jeune couple
hétéro typique, la meuf accrochée au bras de son mec, son mec (qui donne pas
l'impression d'avoir envie d'être ici) en train de scroller sur son ordiphone
sans lui accorder la moindre attention. Il est possible qu'elle mâche un
chewing-gum mais si je me souviens de ça c'est que ça tient du souvenir
inventé.

L'heure du rendez-vous passe, une vieille dame sort de son cabinet avec un air
de petite vieille satisfaite, je me demande si c'est la fin d'une cystite
tenace et je compatis un peu. Enfin on m'appelle et je découvre mon urologue.
Elle est plus jeune que je pensais, ça me fait espérer qu'elle sera moins obtue
qu'un vieux barbon. Elle a l'air dynamique aussi, mais pas pressée. Du style
vite fait bien fait, ce qui est encore un bon point.

Elle ferme la porte, va s'installer derrière son bureau et m'invite à
m'asseoir.

---

Photo by [Dorian Hurst](https://unsplash.com/photos/zoWMVhNvRW4) on Unsplash

[^1]: On dit deadname d'habitude mais morinom ça me fait marrer.
