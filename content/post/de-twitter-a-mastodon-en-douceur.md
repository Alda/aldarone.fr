+++
share = true
comments = false
date = 2018-01-14T12:08:31+01:00
title = "De Twitter à Mastodon en douceur"
image = "img/chris-lawton-leafs.jpg"
slug = "de-twitter-à-mastodon-en-douceur"
+++

Ça fait un petit moment depuis mon [dernier article sur Mastodon][1] et si vous
vous posiez la question, je suis toujours convaincue de ses mérites et de son
potentiel en tant que remplaçant de Twitter.

Pour autant, il y a un paquets de personnes que je cotoyais sur Twitter et qui
me manquent à présent sur Mastodon (même si ça a été l'occasion de rencontrer
tout un tas de nouvelles personnes plus attachantes et géniales les unes que
les autres.)

Rien de nouveau, c'est le lot de toutes les premières utilisatrices de ce genre
de choses, on arrive, il y a personne et on doit construire une communauté à
partir de rien pendant que tout le monde autour nous répète qu'il y a personne
jusqu'à ce qu'enfin il y ai suffisamment de monde.

C'est pour ça que dans cet article j'ai envie de vous expliquer comment nous
filer un coup de main et rejoindre Mastodon sans avoir l'air de rejoindre
Mastodon.

On va faire ça en plusieurs étapes :

- Créer un compte sur une instance Mastodon.
- Enregistrer la correspondance entre notre compte Twitter et Mastodon pour
    que d'autres puissent nous retrouver par ce biais.
- Mettre en place une redirection de nos tweets vers Mastodon.

## Créer son compte sur Mastodon

Si vous avez pas lu d'articles sur le fonctionnement de Mastodon retenez juste
ceci : Mastodon a un fonctionnement similaire à celui de l'email. Chaque
serveur Mastodon fourni une adresse à ses utilisatrices qui peuvent ensuite
l'utiliser pour se contacter entre elles.

La première chose à faire c'est donc de se choisir un fournisseur. Ils sont
tous référencés sur https://instances.social/ où vous n'avez qu'à répondre à
quelques questions sur vos attentes pour obtenir une sélection de fournisseurs
qui vous conviendraient. J'ai aussi fait [une rapide préselection][2] pour vous
faciliter la vie.

Une fois la sélection sous vos yeux, le plus important c'est sans doute <del>la
liberté</del> le nom de domaine puisque de la même manière que si votre
fournisseur de mail c'est `protomail.com` votre adresse mail sera du style
`alda@protonmail.com` si vous vous inscrivez sur https://witches.town/ votre
adresse Mastodon sera du style `TheFameWitch@witches.town` (mais vous pourrez
toujours communiquer avec les gens qui sont chez d'autres fournisseurs.)

## S'inscire dans l'annuaire de correspondance Twitter <=> Mastodon

L'étape suivante c'est d'aller voir si vous avez pas déjà des potes de Twitter sur Mastodon et
vous référencer dans l'annuaire pour permettre aux prochaines de faire de même.

Pour ce faire, rendez-vous sur https://bridge.joinmastodon.org/ et connectez y
votre compte Twitter. Si vous avez déjà des potes référencées, c'est cool ! Et
si vous n'en avez pas, c'est pas grave, il n'y a d'autres gens que vous ne
connaissez pas encore !

Dans les deux cas, vous pouvez maintenant connecter votre compte Mastodon sur
le bridge en cliquant sur le lien « Login via Mastodon » dans l'encadré bleu
sur la même page.

## Poster des tweets sur Mastodon

Enfin, pour commencer à replir votre compte Mastodon sans en avoir l'air, il
existe deux solutions que je vais détailler :

- La solution semi-automatique qui s'aide de Tweetdeck et de deux addons pour le
    navigateur.
- La solution entièrement automatique qui se fait oublier une fois mise en
    place.

### Tweetdeck et compagnie

Cette solution a le désavantage ne ne fonctionner que si vous utilisez
l'interface web Tweetdeck et Mastodon, mais elle a l'avantage de vous permettre
de sélectionner précisément ce que vous voulez envoyer sur votre compte
Mastodon.

Il s'agit d'installer deux addons dans votre navigateur.

Le premier s'appelle Birdsite, est disponible pour [Firefox][3] et [Chrome][4]
et va ajouter un bouton sur l'interface web de Mastodon pour envoyer vos pouets
sur Twitter.

Le second s'appelle Tooter, n'est disponible que sous [Chrome][6] et va ajouter
un bouton sur l'interface de Tweetdeck pour envoyer vos tweets sur Mastodon.

De cette manière, que vous utilisiez Tweetdeck au quotidien ou que vous soyez
de passage sur Mastodon, vous n'aurez qu'à saisir ce que vous voulez envoyer et
le message apparaîtra dans les deux services.

### Tooter sans y penser

Cette solution ci est entièrement automatique et permet d'envoyer tous vos
tweets (sauf les réponses bien sûr) sur Mastodon peu importe les
applications que vous utilisez pour tweeter que ce soit en web, sur votre
smartphone ou votre tablette ou que sais-je encore.

Comme avec le bridge de tout à l'heure, rendez-vous sur
https://crossposter.masto.donte.com.br/ et connectez-y vos comptes Twitter et
Mastodon. Allez ensuite sur [la page d'options][7], défilez jusqu'en bas de la
page et passez l'interrupteur « Twitter -> Mastodon » à `ON`.

Libre à vous de regarder les autres options pour activer l'envoi des RT et des
citations mais dans tous les cas vous êtes prêtes et vos prochains tweets
apparaîtront sur votre compte Mastodon.

## Reprendre une activité normale

Maintenant que vous avez un compte sur Mastodon et que vous avez de quoi y
envoyer des toots sans trop y réfléchir, vous pouvez continuer votre vie comme
si de rien et, qui sait, si vous avez activé les mails de notifications sur
Mastodon vous en recevrez peut-être un de temps en tant, vous indiquant que des
gens comme moi vous suivent et interagissent avec vous.

Et peut-être un jour vous vous direz que ça vaut le coup d'aller y jeter un œil
de manière régulière !

[1]: https://aldarone.fr/welcome-to-mastodon/
[2]: https://instances.social/list#lang=fr&allowed=nudity_all&prohibited=nudity_nocw,pornography_nocw,spam,advertising,spoilers_nocw&users=
[3]: https://addons.mozilla.org/fr-FR/firefox/addon/birdsite/
[4]: https://chrome.google.com/webstore/detail/birdsite/lfmfhaopllgidldpmifdcjdckdggdjaj
[5]: https://addons.mozilla.org/fr-FR/firefox/addon/tooter/
[6]: https://chrome.google.com/webstore/detail/okmlpjijminjkikninbkcnfmhkofgnnk
[7]: https://crossposter.masto.donte.com.br/user

#### Crédit image

Photo by Chris Lawton on Unsplash : <a
style="background-color:black;color:white;text-decoration:none;padding:4px
6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;,
&quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe
UI&quot;, Arial,
sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;"
href="https://unsplash.com/@chrislawton?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge"
target="_blank" rel="noopener noreferrer" title="Download free do whatever you
want high-resolution photos from Chris Lawton"><span
style="display:inline-block;padding:2px 3px;"><svg
xmlns="http://www.w3.org/2000/svg"
style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;"
viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2
4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8
4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3
1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8
2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5
7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span
style="display:inline-block;padding:2px 3px;">Chris Lawton</span></a>
