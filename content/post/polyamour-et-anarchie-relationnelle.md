+++
share = true
comments = false
date = 2017-12-17T11:32:12+01:00
title = "Polyamour et Anarchie Relationnelle"
image = "/img/polyamour-anarchie-relationnelle.jpg"
slug = "polyamour-et-anarchie-relationnelle"
+++

Récemment sur le [Fediverse][1] on m'a posé la question suivante :

> Hey ! Il me semble qu'à un moment il y avait eu un truc sur le polyamour et
t'avais râlé sur le fait que ça parlait de contrat et pas du tout de
responsabilité affective (il me semble que c'était ça :s). Tu pourrais
expliquer un peu ça du coup (ou filer des articles qu'en parle évidemment) ?

Ça m'a semblé être une bonne occasion de parler d'une différence
fondamentale de conception entre le Polyamour et l'Anarchie Relationelle :

Les polyamoureux expliquent souvent qu'on est polyamoureux quand on est
soi-même libre d'aimer plusieurs personnes.

L'anarchie relationnelle a une base beaucoup moins individualiste puisqu'il
s'agit de laisser les autres libres de vivre leurs relations amicales,
sexuelles ou sentimentales comme elles le souhaitent.

Du coup il ne s'agit plus d'envisager les relations comme une série d'accords
mutuels (ou pire, un contrat) mais de les envisager comme un rapport de
confiance au fait que l'autre souhaite notre bonheur et va agir au mieux de ses
moyens.

Comme on nous accorde une confiance, on a la responsabilité de la respecter et
de se montrer à sa hauteur.

Là où le polyamour reste très centré sur les modalités et le soi.

Ça débloque pas mal de trucs en cas de conflit aussi puisqu'on passe d'une
accusation « T'as pas respecté l'accord » / « L'accord ne me convient pas
» à une communication plus positive : « Tel comportement m'a fait souffrir,
comment éviter que ça se reproduise ? »

Pour finir : L'anarchie relationnelle c'est de l'anarchie d'abord. Ça marche
pas si on est pas anarchiste (et les libertariens ne sont pas anarchistes)

Si vous voulez en apprendre plus sur l'Anarchie Relationnelle, je vous invite
à lire [The short instructional manifesto for relationship anarchy][2] ainsi
que [Relationship Anarchy Basics][3].

[1]: https://joinmastodon.org/
[2]: https://app.wallabag.it/share/5ca599fb7c57c6.07910557
[3]: https://app.wallabag.it/share/5ca59a2de7c910.64494343

#### Crédit image

Photo by rawpixel.com on Unsplash : <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@rawpixel?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from rawpixel.com"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">rawpixel.com</span></a>
