+++
comments = false	# set false to hide Disqus
date = "2019-07-28T18:39:15+02:00"
draft = false
image = "/jécris-ton-nom-vasectomie-1-3-la-décision/img/header.jpg"
share = true	# set false to hide share buttons
slug = "jécris-ton-nom-vasectomie-1-3-la-décision"
title = "J'écris ton nom, vasectomie (1/3) : La décision"
+++

C'est l'histoire d'une meuf de trente-deux ans qui a eu une vasectomie il y a un peu
plus de quatre-vingt-seize heures et qui en peu plus de larver sur son canapé en attendant que
ses poils repoussent alors elle décide de raconter comment ça lui est arrivé.

### La décision

Ça commence il y a un peu moins de dix ans, je croyais encore être un jeune vingtenaire
cis-hétéro mais j'étais déjà à peu près certaine de pas vouloir de gosse et
tout à fait convaincue que faire prendre une contraception à vie à ma meuf juste
parce que je veux pas de gamin c'est une attitude plutôt naze.

J'arrive donc à mes vingt-cinq ans, encore un peu jeune pour être absolument
certaine que je veux un truc définitif et je prends une décision : Si arrivée à
trente ans j'ai toujours pas le désir de mioche qui s'est réveillé, ça voudra
dire que les gamins c'est pas pour moi et je passerai à la stérilisation. Après
tout, je le fais bien à mes chattes.

Sauf que voilà, si la vie elle allait tout comme prévu, ça se saurait. Donc à
trente ans, au lieu d'aller voir ma généraliste pour lui dire que je veux
entamer le délai légal de réflexion pour une vasectomie, je vais voir ma
généraliste pour lui dire que je veux commencer une transition hormonale.

!["Mind blowing Millie Bobbie Brown"](https://media.giphy.com/media/3o8dFn5CXJlCV9ZEsg/giphy-downsized.gif)

Comprenez-moi, je venais tout juste de lui balancer mon burn-out au visage,
j'avais *un peu* peur que si je lui parle transition + vasectomie du même coup
j'allais finir internée de force. (On connaît l'amour des médecins pour les
trans)

Avance rapide de deux ans, le sexe queer éclipse tout ce que j'ai pu
connaître du sexe hétéro (et j'en ai vu, vous pouvez me faire confiance) sauf
que, sauf que, autant mes copines trans c'est bon aucun risque de les mettre
enceintes même en cas d'accident de protection, autant mes copains trans et mes
copines cis ça m'a jamais laissée tranquille.

On se prend déjà assez de trucs dans la gueule comme ça on a pas en plus
envie de se faire traiter comme des merdes en pharmacie parce qu'on est allées
chercher une pilule du lendemain.

Ça devient donc urgent de s'y mettre.

---

Photo by [Timothy Eberly](https://unsplash.com/photos/28S1UBUM7aQ) on Unsplash
