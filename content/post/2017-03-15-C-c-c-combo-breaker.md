+++
comments = true
date = "2017-03-15T10:00:00+01:00"
image = "img/letter-mail-mailbox-postbox.jpg"
slug = "C-c-c-combo-breaker"
title = "C- c- c- COMBO BREAKER ! (Signal est indépendant des Google Play Services)"
draft = false
+++

Bon, il semblerait que j'ai abandonné mes articles hebdo alors. « Combien de temps je vais tenir ? » la réponse était donc : trois semaines.

Mais bon, là n'est pas le sujet d'aujourd'hui. Le sujet d'aujourd'hui c'est l'indépendance de Signal vis à vis de Google Play Service.

C'est une critique qui revenait constamment, qui a été la source de beaucoup de FUD, on pouvait ainsi entendre qu'Alphabet avait accès à nos messages chiffrés ou, dans une version plus soft, uniquement aux métadonnées.

En Octobre 2016, [Signal a été mise à l'épreuve de la justice][signal-justice], celles et ceux qui regardaient ont donc pu constater que niveau métadonnées que les oreilles indiscrètes n'ont pas grand chose à se mettre sous la dent.

[signal-justice]: /les-donnees-personelles-enregistrees-par-signal/

Néanmoins, cette dépendance à GCM[^GCM] empêchait tout de même l'installation de Signal sur les smartphones Android ne disposant pas des applis Google. Donc à priori, ceux des gens ayant le plus besoin de protection.

[^GCM]: Google Cloud Messenging

Ce problème est à présent résolu puisque [le mainteneur de Signal a retiré cette dépendance][signal-gcm-dependency] dans la version 3.30.0 permettant ainsi l'installation de Signal sur un système ne disposant pas des Google Play Services.

[signal-gcm-dependency]: https://github.com/WhisperSystems/Signal-Android/commit/1669731329bcc32c84e33035a67a2fc22444c24b

Il restait encore un obstacle à l'adoption de Signal sur les smartphones libérés, en effet l'application n'était disponible que sur le Google Play Store, ce qui nécessitait de passer par ce dernier pour l'installer (et donc d'avoir les Google Play Services sur son smartphone), soit de récupérer l'archive d'installation sur des sites qui ne garantissent pas forcément qu'on est pas entrain de télécharger une version piégée qui, elle, envoie nos données on ne sait où.

Or, [depuis maintenant deux jours][signal-official-comment], il est possible de récupérer une version officielle de Signal en dehors du Google Play Store. Le site officiel de l'application dispose à présent d'une [page permettant de récupérer une archive d'installation][signal-download] qui se mettra automatiquement à jour au fil des nouvelles versions.

[signal-official-comment]: https://github.com/WhisperSystems/Signal-Android/issues/127#issuecomment-286223680
[signal-download]: https://signal.org/android/apk/

Il n'y a plus de raison de ne pas s'y mettre 😉
