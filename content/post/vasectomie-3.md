+++
comments = false	# set false to hide Disqus
date = 2019-07-30T09:24:19+02:00
image = "/jécris-ton-nom-vasectomie-3-3-première-consultation/img/header.jpg"
share = true	# set false to hide share buttons
slug = "jécris-ton-nom-vasectomie-3-3-première-consultation"
title = "J'écris ton nom, vasectomie (3/3) : Première consultation"
+++

C’est l’histoire d’une meuf de trente-deux ans qui a eu une vasectomie il
y a un peu plus de quatre-vingt-seize heures et qui en peu plus de larver sur
son canapé en attendant que ses poils repoussent alors elle décide de
raconter comment ça lui est arrivé.

### Première consultation

Le premier truc que je remarque en entrant dans son cabinet c'est à quel point
il est grand. Et vide. Je me retrouve avec un espace de trois mètres
complètement vide derrière moi. Alors qu'on est à l'orangerie et que j'ose même
pas imaginer la gueule de la taxe foncière.

Sur son bureau en verre il n'y a qu'un iMac (le modèle vingt-sept pouces tant
qu'à faire), le clavier et la souris. Derrière elle : son diplôme encadré de
coupes anatomiques de « l'appareil génital masculin. » À l'époque personne ne
connaît encore GangDuClito et son marketing transphobe pseudo-féministe mais
là, en écrivant, j'ai une pensée émue pour Julia Pietri et je me demande si
elle a un lien avec Julie Pietri (celle qui demandait constamment à Eve de se
lever en 86).

!["Jasmine and Rajah being doubtful"](https://media.giphy.com/media/BulTjvpD7AkTK/giphy-downsized.gif)

Je lui explique vite fait que je viens pour une vasectomie et elle arbore à peu
près le même air confu que sa secrétaire. À tête reposée on en rigole toujours
mais c'est quand même un peu fatigant tous ces gens qui vous regardent sans
arrêt comme une créature étrange et incompréhensible à chaque fois que vous
ouvrez la bouche pour dire un truc.

Heureusement elle se reprend assez vite. Après avoir cligné deux fois des
yeux elle passe en mode automatique. C'est le moment pour le médecin où je
cesse d'être une personne et où je deviens un corps neutre sur lequel on prévoit
une intervention purement technique. Oui, il y a une verge, des testicules, des
canaux déférents, des canaux inguinaux, une vésicule séminale et une
prostate, donc on peut faire une vasectomie sur ce corps sans se poser plus de
questions.

Elle prend le modèle anatomique en plastique qui se trouve derrière elle pour
m'expliquer la procédure et je jette un coup d'œil aux planches anatomiques.
*Appareil génital masculin*. Je fais une note mentale pour me souvenir de
crâmer ces machins sur l'autel du genre quand le moment sera venu.

Elle va faire une incision de chaque côté du scrotum, près de l'entrée des
canaux inguinaux. Là elle va sortir les canaux déférents qui transportent les
spermatozoïdes vers la vésicule séminale où ils seront stockés en attendant la
prochaine éjaculation. Une fois les canaux sorti elle va en couper un bout de
chaque puis ligaturer les deux côtés de la coupure. Elle finira par remettre
tout ça en place et suturer les incisions. J'arriverai en début ou milieu de
journée à la clinique et je repartirai le même jour. Il faudra prévoir quelques
jours de convalescence. Dans les trois mois il faudra avoir régulièrement des
rapports protégés pour évacuer le sperme résiduel qui se trouve dans les canaux
déférents ainsi que dans la vésicule séminale puis faire un spermogramme pour
constater la stérilité.

!["That quote from Dr Who about the big ball of timey-wimey-wibbly-wobbly-stuff"](https://media.giphy.com/media/efU9WbFkGP9NAkLOWn/giphy-downsized.gif)

J'ai donc appris au passage que quand on a des testicules en état de marche
notre corps stocke à peu près de quoi être en capacité d'éjaculer une trentaine
de fois pour se reproduire avant d'être à sec. Ça me semble énorme. Surtout que
ces petites saloperies de spermatozoïdes ont une durée de vie aussi longue.

L'explication technique terminée, elle passe au côté légal. Car il y a un côté
légal. La vasectomie c'est une méthode de contraception définitive et ce
noble état français ne peux pas laisser de pauvres hères inconscients le priver
comme ça de futurs ~~contribuables~~ citoyens.

Légalement je dois m'accorder un délai de réflexion de quatre mois révolus (donc
cinq mois en fait) pour justifier de ma détermination à ne surtout pas
vouloir être en partie responsable d'une grossesse. Aussi, est-ce que je suis
sûre de ne pas vouloir en conserver au Centre d'Étude et de Conservation des
Œufs et du Sperme ?

Là je peux pas m'empêcher de sourire devant sa naïveté. Une meuf qui fait une
conservation de sperme ? Sûrement ce serait la fin de la civilisation telle
qu'on la connait ! En fait sur le sujet je suis déjà renseignée. Si je le
voulais je pourrais effectivement faire une conservation de gamètes au CECOS.
Seulement j'ai aussi envie de changer mon état civil. Et mon numéro de sécurité
sociale.

> **Qui pourra utiliser le sperme conservé ?**\
> L'homme qui a congelé et conservé du sperme et lui seul. Sa présence sera obligatoire pour tout retrait de sperme du CECOS. L’utilisation en assistance médicale à la procréation implique que l’homme et la femme répondent aux règles de l’assistance médicale à la procréation.
>
> **Arrêté du 11 avril 2008 relatif aux règles de bonnes pratiques cliniques et
biologiques d’assistance médicale à la procréation -  chapitre III.4.1.
Information et consentement**\
> "La restitution ultérieure des paillettes n’est faite qu’au patient lui-même."

Vous pouvez être sûres que le CECOS veille scrupuleusement à l'application de
ce principe. C'est les Hommes qui font des conservations de sperme et si
j'acceptai d'être considérée comme un homme le temps du prélèvement et de
l'enregistrement du dossier, le jour où je vais me pointer pour récupérer mes
*paillettes*, on laissera pas une meuf repartir avec son propre sperme.

!["A racoon stealing some cat food and running away on his back feet"](https://media.giphy.com/media/8nLN5DHj6dMic/giphy.gif)

Ça lui fait penser qu'elle est pas certaine de pouvoir légalement pratiquer
cette vasectomie **après** mon changement de numéro de sécurité sociale. Ça me
donne un peu envie d'essayer. Est-ce que c'est ce qui m'a finalement motivée à
déposer le dossier au tribunal ? J'imagine qu'on ne le saura jamais.

Avant de s'occuper de la paperasse elle me propose un examen rapide pour me
montrer où sont mes canaux déférents et surtout vérifier si ils sont bien là.
On passe dans la partie de son cabinet dédiée à ça et je me retrouve sur le dos
à moitié à poil pour me faire examiner la chatte. Elle a pas besoin de chercher
longtemps pour les trouver et me demander si je sens quand elle les touche à
travers la peau.

Je suis surprise quand elle précise que j'en ai bien deux, j'imaginais pas que ça
pouvait être courant de n'en avoir qu'un seul au point que ça puisse être
notable. Un point en plus pour la diversité anatomique des corps humains.
Est-ce que les mecs cis qui apprennent qu'ils ont un seul canal déférent le
vivent mal ?

L'examen se termine juste avant que ça devienne gênant (pour moi en tout cas,
elle doit avoir l'habitude) parce que j'ai beau me concentrer sur l'aspect
médical du truc, ça me fait généralement vite décoller de me faire toucher
cette partie de la chatte dans d'autres contextes et mon cerveau a l'air de se
dire que le moment se prête bien à l'exercice.

!["A cat walking backwards with their head in a paper bag"](https://media.giphy.com/media/WDvOuESz6D9Dy/giphy-downsized.gif)

Une fois rhabillée c'est le moment paperasse. Formulaires de consentement en
deux exemplaires, délais de réflexion à signer et parapher. Voilà monsieur, au
revoir.

Monsieur, évidemment. Il faut bien l'entretenir le petit business de la
dypshorie qui nous pousse par centaines à subir des opérations lourdes dans
l'espoir d'avoir un sexe certifié conforme.

Je retourne devant la secrétaire pour prendre rendez-vous en Mai à la fin du
délai de réflexion et je m'acquitte des quatre-vingt-dix euros pour vingt
minutes de consultation. Reste plus qu'à attendre.

---

Crédit photo : [Jacques Butcher](https://unsplash.com/photos/1pT9JmrsjIY) on
Unsplash
