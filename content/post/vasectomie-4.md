+++
comments = false	# set false to hide Disqus
date = 2019-07-31T15:40:54+02:00
image = "/jécris-ton-nom-vasectomie-4-3-est-ce-que-cest-vraiment-dune-vasectomie-que-je-veux/img/header.jpg"
share = true	# set false to hide share buttons
slug = "jécris-ton-nom-vasectomie-4-3-est-ce-que-c'est-vraiment-dune-vasectomie-que-je-veux"
title = "J'écris ton nom, vasectomie (4/3) : Est-ce que c'est vraiment d'une vasectomie que je veux ?"
+++

C’est l’histoire d’une meuf de trente-deux ans qui a eu une vasectomie il
y a un peu plus de quatre-vingt-seize heures et qui en peu plus de larver sur
son canapé en attendant que ses poils repoussent alors elle décide de
raconter comment ça lui est arrivé.

### Est-ce que c'est vraiment d'une vasectomie que je veux ?

Voilà mi-juin. Juin ? Oui, juin, alors que mon rendez-vous était en mai. Je me
suis pointée un matin de mai au cabinet à l'heure prévue mais avec une semaine
de retard.

Je blâme entièrement mon Trouble de Déficit de l'Attention pour ça.
Évidemment pour comprendre ce qu'il est passé ça a pas été simple, la
secrétaire avait-elle noté mon rendez-vous avec mon morinom ? Mon nom d'usage
?  Le premier prénom sur ma carte d'identité ? Cette fois-ci, l'amoureux qui
m'accompagnait a lui aussi noté la date dans son agenda. Généralement nos
TDAH respectifs se complètent et on s'en sort pas trop mal.

Je me souviens plus de la salle d'attente, je crois que j'ai pas attendu trop
longtemps cette fois, en revanche je me souviens bien de la consultation.

Étant déjà sûre à cent pourcent de vouloir cette vasectomie, j'ai profité du
délai de réflexion pour me concentrer sur une autre opération à cet
endroit. Un truc pas si commun qui fait terriblement peur aux grands
défenseurs de l'ordre genré. L'Orchidectomie Bilatérale ou, plus simplement,
ablation des testicules.

J'aime bien ma chatte comme elle est. Je la connais, on s'entend bien depuis
longtemps et même si les manières de la caresser ont évoluées avec le
Traitement Hormonal de Substitution je sais non seulement comment elle
fonctionne mais je sais aussi expliquer comment elle fonctionne. Donc j'ai
aucune envie de la chambouler en la mettant à l'envers ni de devoir apprendre à
m'en servir à partir de zéro.

Le seul hic là dedans, c'est les petites usines à testo qu'elle contient et qui
m'obligent à prendre de quoi les mettre en sommeil. Comme je suis moyen motivée
pour me cogner dépression (j'en ai déjà assez comme ça merci), thrombose et
méningiôme j'ai choisi de pas prendre de bloqueurs de testo comme l'acétate de
cyprotérone.

Ce médicament est prescrit dans trois cas :

- Cinquante miligrammes par jour dans le traitement du cancer de la prostate.
- Cinquante miligrammes par jour dans le traitement de l'hirsutisme « féminin
  »[^1]
- Cent miligrammes par jour dans le traitement de paraphilies.

Quand une meuf trans va voir un endocrinologue pour un traitement hormonal
c'est par ça qu'il commence. Cent miligrammes d'acétate de cyprotérone par
jour. Ça en dit long sur la manière dont on est considérées par le corps
médical hein ? Et en plus c'est raccord avec le classement de l'*Incongruence
de Genre* dans le même chapitre que les paraphilies par l'Organisation Mondiale
de la Santé.

!["Daffy Duck being insane"](https://media.giphy.com/media/5ZZSQFuNBoMEIrFNXY/giphy.gif)

Je marche pas dans leurs combines et je veux pas me plomber la santé alors
que prendre suffisamment d'œstrogènes et de progestérone ça suffit à couper la
production de testo sans choper une putain de tumeur au cerveau (fut-elle
bégigne et résorbable.)

Encore faut-il que « suffisamment » ait la même définition pour mon corps, pour
moi et pour ma généraliste. Or « suffisamment » pour mon corps c'est
visiblement « trop » pour ma généraliste et aussi « trop » pour moi.

Sous-dosage de progestérone ça veut dire des testicules qui s'arrêtent pas de
faire de la testo, ça veut dire moindre effet de la féminisation due aux
œstrogènes et si en plus on fait du yoyo je vous raconte pas les sautes
d'humeurs.

Sur-dosage de progestérone ça veut dire transpiration excessive et augmentation
du risque du cancer du sein à long terme. Et mon corps pour qu'il se calme il
me faudrait quatre cent miligrammes de progestérone par jour en comprimé à
avaler, et ça mon foie il aime pas du tout donc pour le réconcilier j'en prends
deux cents par jour en suppositoire. Le foie est content il voit rien passer,
moi je flippe de choper un cancer et ma généraliste est pas au courant. On a
des rapports très sains elle et moi à propos de mon traitement.

!["David Bowie making « shhh »"](https://media.giphy.com/media/V7ILmYEggz1p6/giphy.gif)

Après avoir réfléchi en ces termes, la solution m'apparaît assez clairement et
me faire retirer les gonades c'est tout bénef. Déjà ça prendra moins de
place dans les culottes, ensuite j'aurai plus à craindre une remontée de
testostérone, je pourrai baisser les doses d'hormones et puis ça changera pas
fondamentalement le fonctionnement de ma chatte.

Quand mon urologue me fait asseoir en face d'elle et qu'elle me demande si j'ai
réfléchi, si j'en ai discuté avec ma compagne (Heu, pardon ?) et si je suis
sûre de moi, j'exprime un franc « oui mais… » parce que ce serait quand même
plus pratique de prévoir une orchidectomie maintenant que je sais que j'en veux
une plutôt qu'une vasectomie puis une orchidectomie, j'ai pas envie de passer
ma vie sur le billard.

Manque de bol ça la braque un peu. Elle aussi elle est incapable d'imaginer
qu'une meuf trans ne veuille pas la Sacro-Sainte vaginoplastie qui lui
donnera accès à un Sexe Certifié Féminin (un trou à pénétrer, en gros). Donc
elle fait pas ça.

Bien sûr c'est pas sa faute, c'est l'Assurance Maladie qui acceptera jamais de
rembourser, pour ce genre d'opération elle me dit qu'il faut passer par les
médecins de l'Hôpital Civil, c'est eux qui se spécialisent dans les changements
de sexe par contre elle sait pas si ils voudront retirer les testicules en
conservant la verge.

Les mots « mon corps mon choix », « auto-détermination », « choix du médecin »,
« respect des patient⋅e⋅s » et « dépathologisation » résonnent dans
ma tête alors que je réalise du même coup que je mets même des points
médians dans mes pensées. Je me souviens aussi de ce psychiatre que j'ai
entendu prononcer « délire pseudo-hermaphrodite » en parlant de meufs trans qui
prenaient des hormones sans vouloir d'opération génitale. Acétate de
Cyprotérone. Paraphilie.

Tant pis pour elle, je me contenterai de me faire couper les canaux déférents,
je trouverai une solution pour mes gonades l'année prochaine. J'inviterai
Lukas Dhont à assister à l'opération, peut-être ça le fera bander.

!["Hermione Granger doing an eye roll"](https://media.giphy.com/media/OUwzqE4ZOk5Bm/giphy.gif)

On prévoit ça fin juillet, le vingt-quatre. Le jour de l'anniversaire de mon
petit frère, est-ce que c'est symbolique ? Entre temps, je dois prendre
rendez-vous chez un anésthésiste et faire ma préadmission à la clinique. C'est
dans un mois.

[^1]: C'est officiellement une maladie d'avoir trop de poils. C'est d'ailleurs
      comme ça qu'on se fait rembourser nos séances d'épilation longue durée:
      Hirsutisme.

---

Photo by [Andrew Pons](https://unsplash.com/@imandrewpons?utm_source=unsplash&utm_medium=referral &utm_content=creditCopyText)
on [Unsplash](https://unsplash.com/search/photos/dog?utm_source=unsplash&utm_medium =referral&utm_content=creditCopyText)
