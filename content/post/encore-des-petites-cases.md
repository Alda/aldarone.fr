+++
title = "Encore des petites cases"
slug = "encore-des-petites-cases"
image = "/img/samuel-zeller-367971-unsplash.jpg"
comments = false	# set false to hide Disqus
share = true	# set false to hide share buttons
date = 2018-05-20T11:09:38+02:00
+++

Il y a pas longtemps j'ai écrit [un article à propos de cases][cases] il est pas
ouf, voire un peu nul et naïf.

[cases]: https://aldarone.fr/des-petites-cases/

Mais il m'a aidé a avancer dans ma réflexion sur cette idée de cases.

J'estime maintenant que parler de « cocher des cases pour soi-même » est une
grosse [erreur fondamentale d'attribution][wiki].

[wiki]: https://fr.m.wikipedia.org/wiki/Erreur_fondamentale_d'attribution

Cette erreur relève du biais cognitif, ce qui signifie que nous (occidentaux
au moins) y sommes généralement vulnérables, et consiste à accorder plus
d'importance aux caractéristiques internes d'une personne (qui elle est, ce en
quoi elle croit, etc.) plutôt qu'à des caractéristiques externes (la société
dans laquelle elle évolue, etc.)

Son influence peut-être ignorée, comme dans le cas d'une personne qui se
cherche plus où moins désespérément et va se raccrocher à un mot décrivant ce
qu'elle pense être son identité parce qu'elle se retrouve dans les personnes
qui y sont rattachées également.

Ou le biais peut-être instrumentalisé, comme dans le cas d'un discours
individualiste visant à faire peser la responsabilité des oppressions et/ou
stigmatisations sur les personnes oppressées et/ou stigmatisées.

Ce dernier le fait d'ailleurs en s'appuyant sur les personnes qui sont dans le
premier cas, en s'aidant de cette espèce de tendance actuelle qui pousse à
[découper nos identités][3] de manière [toujours plus spécifique][4] et en la
renforçant.

[3]: http://u1kwao9i7u9xyle2hibw4g6h.wpengine.netdna-cdn.com/wp-content/uploads/2015/03/Genderbread-Person-3.3.jpg
[4]: https://img00.deviantart.net/29ab/i/2010/282/b/e/sexual_attraction_v2_by_nelde-d2zbi2j.png

L'identité est quelque chose de strictement personnel, pas au sens où il
faudrait la garder pour soi mais au sens où elle n'est communiquable aux
autres que de manière très incomplète.

Ça explique notamment le paradoxe et la vanité de cette quête de reconnaissance
au travers de définitions toujours plus spécifiques[^1] et également pourquoi
ce ne sont pas les identités (quelles qu'elles soient) qui causent notre
rattachement par la société à un groupe social.

[^1]: Plus la définition est spécifique, moins elle concerne de gens et plus on se retrouve isolé dans son expérience de cette identité.

Je crois que personne ne se réveille le matin en se demandant dans quelle case
elle va bien décider de se mettre aujourdhui.

La réalité est bien plus complexe et relève de *la nécessité* de **composer
avec les pratiques** qui s'accordent le mieux à la manière dont nous nous
percevons, dont nous voulons être perçues **et avec le prix que la société va
nous faire payer** si ces pratiques nous font tomber sous le coup d'une
oppression et/ou d'une stigmatisation.

Fondamentalement, ce sont les pratiques et les manières dont elles sont
perçues par la société qui causent notre assignation à un groupe social et non
pas la participation à un groupe social qui serait la cause de nos pratiques[^2].

[^2]: Et ce, même si une part de nos pratiques peut relever de la performance dans le cadre de notre assignation à un groupe.

Pour donner l'exemple d'un homme homosexuel, ce dernier n'a ou ne désire pas
de relations sexuelles et/ou sentimentales avec des hommes *parce qu'il est
homosexuel* mais il est homosexuel *parce qu'il a ou désire des relations
sexuelles et/ou sentimentales avec des hommes.*

Ce sont ses pratiques (avoir ou désirer des relations sentimentales et/ou
sexuelles avec des hommes) qui vont faire que la société va le percevoir comme
membre du groupe social « *homosexuel* » et qui va le traiter en tant que tel,
avec toutes les intimidations et menaces de violences que ça implique.

#### Crédit image

Photo by Samuel Zeller on Unsplash:
<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@samuelzeller?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Samuel Zeller"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Samuel Zeller</span></a>
