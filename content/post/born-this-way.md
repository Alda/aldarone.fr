+++
title = "Born This Way"
slug = "born-this-way"
image = "/img/born-this-way/header.jpg"
share = true # set false to hide share buttons
date = 2019-06-27T14:56:11+02:00
+++

J'en ai gros ce matin alors voilà une réflexion sur le besoin de se
considérer soi même autrement que comme "né comme ça"

Cette théorie de l'absence de choix dans l'identité de genre est calquée sur
l'argument de l'absence de choix dans l'orientation sexuelle.

C'est un argument qui a eu son utilité au commencement des luttes
homosexuelles qui était opposé à la forte pression religieuse. Les
homosexuels étaient accusés de "faire le choix" d'adopter des pratiques
déviantes et de pécher en toute conscience. Il était alors efficace de leur
opposer que "Dieu nous a fait ainsi", indiquant une absence de choix.

Néanmoins, cet argument pose le problème d'être un essentialisme validant
intrinsèquement le système du genre qui sert le patriarcat.

S'il est naturel de naître hétérosexuel ou de naître homosexuel, il est
naturel d'être attiré uniquement par les hommes ou d'être attiré uniquement
par les femmes. Or, si l'attirance envers un genre défini est naturel, comment
ce genre défini pourrait-il ne pas l'être également ?

L'idée même selon laquelle on naît avec une identité de genre et une
orientation sexuelle est profondément naturalisante et anti-féministe, et il
serait temps de l'abandonner.
