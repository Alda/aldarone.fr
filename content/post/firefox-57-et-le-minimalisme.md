+++
share = true
comments = false
date = "2017-11-15T20:13:46+01:00"
title = "Comment j'ai minimalisé l'interface de Firefox 57"
image = "/img/sarah-dorweiler-211779.jpg"
slug = "comment-j'ai-minimalisé-l'interface-de-firefox-57"
+++

J'ai profité de la sortie de Firefox Quantum cette semaine pour peaufiner un peu mon profil et mieux l'adapter à mon usage. Voici le résultat obtenu à l'aide de quelques webextensions et personnalisations :

![Firefox Quantum Minimalist Interface](/img/firefox-minimalism.png)

# Vim c'est un peu la vie

En premier lieu, comme j'ai appris à utiliser Vim, je peux gagner un temps significatif à l'aide de l'extension [Vivimum][1] qui booste les raccourçis clavier en permettant une utilisation de Firefox similaire à celle de Vim.

Ouvrir des liens en pressant quelques touches, lancer une recherche avec une autre, passer d'onglet en onglet sans avoir à bouger les mains pour attraper la souris ou glisser sur le touchpad est quelque chose dont je n'imagine même plus me passer.

# `about:newtab` est juste USELESS

Le second point découle un peu du premier, quand j'ouvre un nouvel onglet, c'est la page `about:newtab` qui s'affiche et elle ne m'est d'absolument aucune utilité.

![Firefox about:newtab page](/img/firefox-57-highlights.jpg)

La barre de recherche est redondante avec la barre d'adresse qui permet déjà de lancer une recherche. Et les sites récents et plus visités me sont déjà accessibles dans cette même bare d'adresse que ce soit par mes favoris ou par mon historique.

Je l'ai donc remplacée à l'aide de l'extension [tabliss][2] qui m'affiche une horloge et une salutation en fonction de l'heure du jour, la météo dans le coin supérieur droit et une magnifique photo aléatoire provenant du site [Unsplash][3].

# Tu peux le personnaliser avec des jolies couleurs

Ensuite, même si je n'aime pas Vivaldi (parce que quel est l'intérêt d'un énième navigateur privateur encore basé sur Chrome ?) je trouve amusant et agréable sa capacité à changer la couleur de son interface en fonction de la page en cours de consultation.

Pour Firefox, c'est l'extension [VivaldiFox][4] qui permet d'apporter cette fonctionnalité.

# Gagner de la place en cachant la barre d'onglets

Enfin, utilisant [Vimium][1], la barre d'onglet ne m'est, elle aussi, plus d'aucune utilité. Je peux passer à l'onglet de mon choix en pressant la touche <kbd>T</kbd> ou en les faisant défiler avec <kbd>CTRL</kbd> + <kbd>TAB</kbd>, <kbd>J</kbd> ou <kbd>K</kbd> donc je peux me permettre de complètement cacher cette barre pour gagner un peu d'espace vertical.

Ici c'est un peu plus complexe qu'installer une extension. Il faut accéder à la page `about:profiles` puis ouvrir le répertoire du profil courant. À l'intérieur on va créer le dossier `chrome` et dans ce dossier, un fichier nommé `userChrome.css` qui contiendra le code suivant :

```css
@namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");

#TabsToolbar {
    visibility:collapse;
}
```

Après un redémarrage du nivagateur, la barre d'onglet a alors disparu.

Un problème se pose alors : On ne sait plus combien d'onglet ouvert on a et si on ferme le dernier, c'est tout le navigateur qui se ferme du même coup.

Pour éviter ça, il suffit de passer la directive `browser.tabs.closeWindowWithLastTab` à `false` dans la page `about:config`, en fermant le dernier onglet on arrivera alors sur un onglet vide (qui affiche donc [tabliss][2]).

Et voilà comment grâce à la simplicité apportée par les WebExtension, la vitesse de Quantum et le nouveau thème nommé Photon on arrive à une interface qui laisse la part belle au plus important : Le contenu.

*Crédit photo* : <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@sarahdorweiler?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Sarah Dorweiler"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title></title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Sarah Dorweiler</span></a>

[1]: https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/
[2]: https://tabliss.io/
[3]: https://unsplash.com/
[4]: https://addons.mozilla.org/en-US/firefox/addon/vivaldifox/
