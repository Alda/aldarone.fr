+++
date = "2017-05-01T12:11:59+02:00"
image = "img/mist.jpg"
share = false
slug = "rapport-de-transparence-avril-2017"
title = "Rapport de transparence Avril 2017"
+++

Salut les sorcières !

L'instance Mastodon [Witches Town](https://witches.town/) est ouverte depuis le 03 Avril et comme vous êtes déjà quelques un⋅es à m'aider à la financer et que je commence à recevoir les factures, c'est l'heure du premier rapport de transparence où je détaillerais les dons reçus et les dépenses engagées.

| Description | Crédit | Débit |
| ---- | ------- | ---- |
| Nom de domaine witches.town |  | -7.42€ |
| Gargron's Patreon |  | -11.40€ |
| Scaleway hosting |  | -54.94€ |
| Liberapay donations | +92.28€ | -0.50€ |
| **Sous total** | +92.28€ | -74.26€ |
| **Total** | +18.02€ | |

Les inscriptions sont toujours ouvertes le soir entre 20h et 21h (heure de Paris) et pour me soutenir dans la gestion de l'instance, n'hésitez pas à me faire un don sur [Liberapay](https://liberapay.com/Alda/). Et si vous le faites déjà, merci à vous, vous êtes des cœurs.
